# Custom bashrc.d

## Cloning the Repository

If you have anything in your `~/.bashrc.d` directory that you would like to keep (for example aliases you have set up before cloneing this project), please save them somewhere.
Perhaps by creating a backup of your `~/.bashrc.d` folder by running `mv ~/.bashrc.d ~/.bashrc.d.bak`.

Then run `git clone git@gitlab.com:ccrpc/bashrc.d.git ~/.bashrc.d` to clone this project into your `~/.bashrc.d directory`.

If you would like to add user specific aliases and or settings, please create a file named `user-specific` in this folder and add them there.

If you use these aliases / settings often and believe other developers would benefit from having them, consider opening an issue and a merge request to have them added to this repository!


## Setting Authentication Variables

Create a file named `auth` in the repository with the following format
```bash
# Set gitlab auth token for several projects
export GITLAB_AUTH_TOKEN=replaceme
export GITLAB_USERNAME=replaceme
export INFRACOST_API_KEY=replaceme
```

Replace these with your information
#TODO give better instructions here

## Setting up the Loop

To have your `.bashrc` script this repository and run the scripts within, add this loop to the end of it:
```bash
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        # Skip files ending with .sh, /example., .md, and .gitignore
        if [[ ! $rc =~ .*\.(sh|example|gitignore|md) ]] && [ -f "$rc" ]; then
           . "$rc"
        fi
    done
fi
unset rc
```

If your bashrc file already has such a loop (which is the case on Bluefin), the only necessary change is the if statement to keep bash from attempting to run the readme as a bash script.

That is to say replacing
```bash
. "$rc"
```

With

```bash
# Skip files ending with .sh, /example., .md, and .gitignore
if [[ ! $rc =~ .*\.(sh|example|gitignore|md) ]] && [ -f "$rc" ]; then
    . "$rc"
fi
```

An example of a working `.bashrc` file is given in `.bashrc.example`

## Executing the Changes:

If you want the the changes to be reflected in an already open terminal, run `source ~/.bashrc` in that terminal.

If you open up a new terminal the .bashrc will be run automatically.

See also [documentation on how bashrc works](https://wiki.archlinux.org/title/bash)